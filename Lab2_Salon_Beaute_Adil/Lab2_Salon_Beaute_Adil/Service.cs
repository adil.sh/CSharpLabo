﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Salon_Beaute_Adil
{
    class Service
    {
        private int cnt;
        private int idService;
        private string nom;
        private double prix;

        //Constructor
        public Service() { cnt++; }

        public Service(int idservice,string nom, double prix)
        {
            this.idService = idservice;
            this.nom = nom;
            this.prix = prix;

            cnt++;
        }

        // Getters

        public int ID
        {
            get { return this.idService; }
            set { this.idService = value; }
        }

        public string NOM
        {
            get { return this.nom; }
            set { this.nom = value; }
        }

        public double PRIX
        {
            get { return this.prix; }
            set { this.prix = value; }
        }

    }
}
