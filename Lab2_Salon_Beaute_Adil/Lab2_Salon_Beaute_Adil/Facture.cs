﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Salon_Beaute_Adil
{
    class Facture
    {
        private double prixTotale;
        private double rabaispProduits;
        private double rabaisServices;
        private double totalProduits;
        private double totaleServices;

        public double PRIXTOTAL
        {
            get { return this.prixTotale; }
            set { }
        }

        public double RABAISPRODUITS
        {
            get { return this.rabaispProduits; }
            set {
                rabaispProduits = value;
            }
        }

        public double RABAISSERVICES
        {
            get { return this.rabaisServices; }
            set { }
        }

        public double TOTALPRODUITS
        {
            get { return this.totalProduits; }
            set
            {
                totalProduits = value;
            }
        }

        public double TOTALSERVICES
        {
            get{ return this.totaleServices; }
            set
            {
                totaleServices = value;
            }
        }

        // Methods

        public Facture(double totaleProduits, double totalServices, double rabaisProduits, double rabaisServices)
        {
            this.totalProduits = totaleProduits;
            this.totaleServices = totalServices;
            this.rabaispProduits = rabaisProduits;
            this.rabaisServices = rabaisServices;      
        }

        public Facture(double totaleproduits)
        {
            this.totalProduits = totaleproduits;
        }
        public Facture()
        {
         
        }
    }
}
