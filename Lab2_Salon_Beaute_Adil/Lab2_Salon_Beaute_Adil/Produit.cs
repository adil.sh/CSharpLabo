﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Salon_Beaute_Adil
{
    class Produit
    {
        private int cint;
        private int idProduit;
        private string nom;
        private double prix;

        // Empty constructor
        public Produit()
        { }
        // Construct with values
        public Produit(int idProduit,string nom, double prix)
        {
            this.idProduit = idProduit;
            this.nom = nom;
            this.prix = prix;
        }
        //getter & setter
        public int ID
        {
            get {return this.idProduit; }
            set { this.idProduit = value; }
        }

        public string NOM
        {
            get { return this.nom; }
            set{ this.nom = value; }
        }

        public double PRIX
        {
            get { return this.prix; }
            set { this.prix = value; }
        }


    }
}
