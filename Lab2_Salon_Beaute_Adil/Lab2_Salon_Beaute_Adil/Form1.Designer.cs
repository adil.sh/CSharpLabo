﻿namespace Lab2_Salon_Beaute_Adil
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxMembership = new System.Windows.Forms.ComboBox();
            this.txt_prenom_client = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_nom_client = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.listBoxService = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.listBoxProduit = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listBoxClient = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblRabaisS = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblRabaisP = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbltotalservices = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblTotalProduit = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.listBoxpanierS = new System.Windows.Forms.ListBox();
            this.listBoxpanierP = new System.Windows.Forms.ListBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.listBoxFacture = new System.Windows.Forms.ListBox();
            this.lblFactureC = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.comboBoxMembership);
            this.panel1.Controls.Add(this.txt_prenom_client);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txt_nom_client);
            this.panel1.Location = new System.Drawing.Point(21, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(233, 228);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(228, 59);
            this.button1.TabIndex = 6;
            this.button1.Text = "CREATE CLIENT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(116, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "MEMBERSHIP:";
            // 
            // comboBoxMembership
            // 
            this.comboBoxMembership.FormattingEnabled = true;
            this.comboBoxMembership.Items.AddRange(new object[] {
            "1.PREMIUM",
            "2.GOLD",
            "3.BRONZE",
            "4.NO MEMBERSHIP"});
            this.comboBoxMembership.Location = new System.Drawing.Point(85, 108);
            this.comboBoxMembership.Name = "comboBoxMembership";
            this.comboBoxMembership.Size = new System.Drawing.Size(141, 21);
            this.comboBoxMembership.TabIndex = 4;
            // 
            // txt_prenom_client
            // 
            this.txt_prenom_client.Location = new System.Drawing.Point(85, 49);
            this.txt_prenom_client.Name = "txt_prenom_client";
            this.txt_prenom_client.Size = new System.Drawing.Size(141, 20);
            this.txt_prenom_client.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "PRENOM:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "NOM:";
            // 
            // txt_nom_client
            // 
            this.txt_nom_client.Location = new System.Drawing.Point(85, 10);
            this.txt_nom_client.Name = "txt_nom_client";
            this.txt_nom_client.Size = new System.Drawing.Size(141, 20);
            this.txt_nom_client.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "CREATION DE CLIENT";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.listBoxService);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.listBoxProduit);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.listBoxClient);
            this.panel2.Location = new System.Drawing.Point(291, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(590, 228);
            this.panel2.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(394, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "SERVICES";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "PRODUITS";
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(397, 189);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(186, 36);
            this.button3.TabIndex = 5;
            this.button3.Text = "Ajouter au Panier";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // listBoxService
            // 
            this.listBoxService.FormattingEnabled = true;
            this.listBoxService.Location = new System.Drawing.Point(397, 23);
            this.listBoxService.Name = "listBoxService";
            this.listBoxService.Size = new System.Drawing.Size(186, 160);
            this.listBoxService.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(205, 189);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(186, 36);
            this.button2.TabIndex = 3;
            this.button2.Text = "Ajouter au Panier";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listBoxProduit
            // 
            this.listBoxProduit.FormattingEnabled = true;
            this.listBoxProduit.Location = new System.Drawing.Point(205, 23);
            this.listBoxProduit.Name = "listBoxProduit";
            this.listBoxProduit.Size = new System.Drawing.Size(186, 160);
            this.listBoxProduit.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "CLIENT";
            // 
            // listBoxClient
            // 
            this.listBoxClient.FormattingEnabled = true;
            this.listBoxClient.Location = new System.Drawing.Point(7, 23);
            this.listBoxClient.Name = "listBoxClient";
            this.listBoxClient.Size = new System.Drawing.Size(186, 160);
            this.listBoxClient.TabIndex = 0;
            this.listBoxClient.SelectedIndexChanged += new System.EventHandler(this.listBoxClient_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(291, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(193, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "ACHAT DE PRODUITS ET SERVICES";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 272);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "PANIER D\'ACHAT";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel3.Controls.Add(this.lblRabaisS);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.lblRabaisP);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.lbltotalservices);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.lblTotalProduit);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.listBoxpanierS);
            this.panel3.Controls.Add(this.listBoxpanierP);
            this.panel3.Location = new System.Drawing.Point(21, 288);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(590, 189);
            this.panel3.TabIndex = 10;
            // 
            // lblRabaisS
            // 
            this.lblRabaisS.AutoSize = true;
            this.lblRabaisS.Location = new System.Drawing.Point(493, 103);
            this.lblRabaisS.Name = "lblRabaisS";
            this.lblRabaisS.Size = new System.Drawing.Size(0, 13);
            this.lblRabaisS.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(490, 103);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(399, 103);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Rabais Services:";
            // 
            // lblRabaisP
            // 
            this.lblRabaisP.AutoSize = true;
            this.lblRabaisP.Location = new System.Drawing.Point(490, 43);
            this.lblRabaisP.Name = "lblRabaisP";
            this.lblRabaisP.Size = new System.Drawing.Size(0, 13);
            this.lblRabaisP.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(400, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Rabais Produits:";
            // 
            // lbltotalservices
            // 
            this.lbltotalservices.AutoSize = true;
            this.lbltotalservices.Location = new System.Drawing.Point(475, 81);
            this.lbltotalservices.Name = "lbltotalservices";
            this.lbltotalservices.Size = new System.Drawing.Size(0, 13);
            this.lbltotalservices.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(399, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Total Services: ";
            // 
            // lblTotalProduit
            // 
            this.lblTotalProduit.AutoSize = true;
            this.lblTotalProduit.Location = new System.Drawing.Point(475, 21);
            this.lblTotalProduit.Name = "lblTotalProduit";
            this.lblTotalProduit.Size = new System.Drawing.Size(0, 13);
            this.lblTotalProduit.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(399, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Total Produits:";
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(402, 133);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(167, 48);
            this.button6.TabIndex = 8;
            this.button6.Text = "Prix Total";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(192, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "PANIER SERVICES";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(0, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "PANIER PRODUITS";
            // 
            // listBoxpanierS
            // 
            this.listBoxpanierS.FormattingEnabled = true;
            this.listBoxpanierS.Location = new System.Drawing.Point(195, 21);
            this.listBoxpanierS.Name = "listBoxpanierS";
            this.listBoxpanierS.Size = new System.Drawing.Size(186, 160);
            this.listBoxpanierS.TabIndex = 4;
            // 
            // listBoxpanierP
            // 
            this.listBoxpanierP.FormattingEnabled = true;
            this.listBoxpanierP.Location = new System.Drawing.Point(3, 21);
            this.listBoxpanierP.Name = "listBoxpanierP";
            this.listBoxpanierP.Size = new System.Drawing.Size(186, 160);
            this.listBoxpanierP.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel4.Controls.Add(this.lblTotal);
            this.panel4.Controls.Add(this.lblFactureC);
            this.panel4.Controls.Add(this.listBoxFacture);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Location = new System.Drawing.Point(646, 288);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(228, 192);
            this.panel4.TabIndex = 12;
            this.panel4.Visible = false;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(51, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(134, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "SALON DE BEAUTE ADIL";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(75, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "VOTRE FACTURE";
            // 
            // listBoxFacture
            // 
            this.listBoxFacture.FormattingEnabled = true;
            this.listBoxFacture.Location = new System.Drawing.Point(26, 69);
            this.listBoxFacture.Name = "listBoxFacture";
            this.listBoxFacture.Size = new System.Drawing.Size(181, 82);
            this.listBoxFacture.TabIndex = 2;
            // 
            // lblFactureC
            // 
            this.lblFactureC.AutoSize = true;
            this.lblFactureC.Location = new System.Drawing.Point(20, 43);
            this.lblFactureC.Name = "lblFactureC";
            this.lblFactureC.Size = new System.Drawing.Size(0, 13);
            this.lblFactureC.TabIndex = 3;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(23, 168);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(0, 13);
            this.lblTotal.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 492);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "x`";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxMembership;
        private System.Windows.Forms.TextBox txt_prenom_client;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_nom_client;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox listBoxClient;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBoxProduit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListBox listBoxService;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListBox listBoxpanierS;
        private System.Windows.Forms.ListBox listBoxpanierP;
        private System.Windows.Forms.Label lblTotalProduit;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label lbltotalservices;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblRabaisP;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblRabaisS;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ListBox listBoxFacture;
        private System.Windows.Forms.Label lblFactureC;
        private System.Windows.Forms.Label lblTotal;
    }
}

