﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Lab2_Salon_Beaute_Adil
{
    public partial class Form1 : Form
    {

       // Produit produitPannier;
        ArrayList Serviceslist = new ArrayList();
        ArrayList Produitlist = new ArrayList();
        ArrayList ProduitAchetr = new ArrayList();
        ArrayList serviceAchetr = new ArrayList();
        // private Client[] clientData;
        private List<Client> clientList;
        int nombreItems = 0;
        public Form1()
        {
            InitializeComponent();
            // clientData = new Client[];
            clientList = new List<Client>();
           // produitPannier = new Produit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((txt_nom_client.Text == "") || (txt_prenom_client.Text == "") || (comboBoxMembership.SelectedIndex == -1))
            {
                MessageBox.Show("SVP Entrer le Nom, Prenom et Choisir le Membership pour Client");
            }
            else
            {
                Client c1 = new Client(txt_nom_client.Text, txt_prenom_client.Text, comboBoxMembership.Text);

                // clientData[Client.nbClients - 1] = c1;
                clientList.Add(c1);

                foreach (Client element in clientList)
                {
                    listBoxClient.Items.Add( c1.NOM + " " + c1.MEMBERSHIP);
                }

                //MessageBox.Show(c1.NOM + c1.PRENOM + c1.MEMBERSHIP);
                membershipTypecheck();

                //enable buttons
                button2.Enabled = true;
                button3.Enabled = true;
            }
        }

        public double membershipTypecheck()
        {
         
            if (comboBoxMembership.Text == "1.PREMIUM")
            { 
                return 0.15;
            }
            else if (comboBoxMembership.Text == "2.GOLD")
            {
                return 0.1;
            }
            else if (comboBoxMembership.Text == "3.BRONZE")
            {
                return 0.05;
            }
            else
            {
                return 0;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadProduitsXML();
            LoadServicesXML();

        }

        // Method load produits apartir de ficher xml
        public void LoadProduitsXML()
        {

            // LOADING PRODUITS FROM XML TO LISTBOX PRODUITS


            // Charger le array list a partir de document xm

            XmlDocument doc = new XmlDocument();

            doc.Load(Application.StartupPath + @"/ProduitXML.xml");

            //MessageBox.Show(doc.InnerXml);
            // on prend balise de type prof et sous balise comme id, prenom, nom
            XmlNodeList nodeList = doc.GetElementsByTagName("produit");

            foreach (XmlNode element in nodeList)
            {
                Produit tempProduit = new Produit();
                tempProduit.ID = Convert.ToInt32(element.ChildNodes[0].InnerText);
                tempProduit.NOM = (element.SelectSingleNode("nom")).InnerText;
                tempProduit.PRIX = Convert.ToDouble((element.SelectSingleNode("prix")).InnerText);


                Produitlist.Add(tempProduit);

            }


            foreach (Produit element in Produitlist)
            {
                listBoxProduit.Items.Add(element.NOM);

            }

        }

        public void LoadServicesXML()
        {

            // LOADING PRODUITS FROM XML TO LISTBOX PRODUITS


            // Charger le array list a partir de document xm

            XmlDocument doc1 = new XmlDocument();

            doc1.Load(Application.StartupPath + @"/ServiceXML.xml");

            //MessageBox.Show(doc.InnerXml);
            // on prend balise de type prof et sous balise comme id, prenom, nom
            XmlNodeList nodeList = doc1.GetElementsByTagName("service");

            foreach (XmlNode element in nodeList)
            {
                Service tempProduit = new Service();
                tempProduit.ID = Convert.ToInt32(element.ChildNodes[0].InnerText);
                tempProduit.NOM = (element.SelectSingleNode("nom")).InnerText;
                tempProduit.PRIX = Convert.ToDouble((element.SelectSingleNode("prix")).InnerText);


                Serviceslist.Add(tempProduit);


            }


            foreach (Service element in Serviceslist)
            {
                listBoxService.Items.Add(element.NOM);

            }

        }
      

        private void button2_Click(object sender, EventArgs e)
        {
            nombreItems++;
            if (listBoxClient.SelectedIndex == -1)
            { MessageBox.Show("SVP Choisir un client avant de ajouter au panier"); }
            else
            {

                //  listBoxpanierP.Items.Add(listBoxProduit.SelectedItem);
                foreach (Produit element in Produitlist)
                {
                    if ((string)listBoxProduit.SelectedItem == element.NOM)
                    {
                        listBoxpanierP.Items.Add(element.NOM + " " + element.PRIX + " $");
                        listBoxFacture.Items.Add(element.NOM + " \t" + element.PRIX + " $");

                        ProduitAchetr.Add(element);


                    }
                }
                button6.Enabled = true;
            }
        }
       // Ajouter service
        private void button3_Click(object sender, EventArgs e)
        {

            if (listBoxClient.SelectedIndex == -1)
            {
                MessageBox.Show("SVP Choisir un client avant de ajouter au panier");
            }
            else
            {
                //  listBoxpanierP.Items.Add(listBoxProduit.SelectedItem);
                foreach (Service element in Serviceslist)
                {
                    if ((string)listBoxService.SelectedItem == element.NOM)
                    {

                        listBoxpanierS.Items.Add(element.NOM + " " + element.PRIX + " $");
                        listBoxFacture.Items.Add(element.NOM + " \t" + element.PRIX + " $");
                        serviceAchetr.Add(element);
                    }

                }
                button6.Enabled = true;
            }
        }
        // GET TOTAL BUTTON, WILL DISPLAY BOTH PRODUCTS AND SERVICE TOTAL, INCLUDING DISCOUNTS
        private void button6_Click(object sender, EventArgs e)
        {
            getProduiTotale();
            getServiceTotale();
           
            getTotaleFinale();
            panel4.Visible = true;
        }

        // Method to get the total of all the products in the Cart
        double prixFinalePRab;
        public void getProduiTotale()
        {
            Facture f2 = new Facture();
            foreach (Produit element in ProduitAchetr)
            {
                f2.TOTALPRODUITS += element.PRIX;
            }
            lblTotalProduit.Text = f2.TOTALPRODUITS.ToString() + " $";

            double rabaisP = membershipTypecheck();
            double rabaisTotale = f2.TOTALPRODUITS * rabaisP;
            rabaisTotale = Math.Round(rabaisTotale, 2);

            lblRabaisP.Text = rabaisTotale.ToString() + " $";

            prixFinalePRab = (f2.TOTALPRODUITS - rabaisTotale);


        }
        double prixFinaleSavecRabais;
        // Monthod to get servicetotal
        public void getServiceTotale()
        {
            Facture f3 = new Facture();
            foreach (Service element in serviceAchetr)
            {
                f3.TOTALSERVICES += element.PRIX;
            }
           
            lbltotalservices.Text = f3.TOTALSERVICES.ToString() + " $";
            double rabaisS = membershipTypecheck();
            double rabaisTotale = f3.TOTALSERVICES * rabaisS;
            rabaisTotale = Math.Round(rabaisTotale, 2);

            lblRabaisS.Text = rabaisTotale.ToString() + " $";
            prixFinaleSavecRabais = (f3.TOTALSERVICES - rabaisTotale);
        }

        //Method to get produit rabais
        public void getTotaleFinale()
        {
            double prixFinale = prixFinalePRab + prixFinaleSavecRabais;
            prixFinale  = Math.Round(prixFinale, 2);
            button6.Text = "Prix Total = " + prixFinale.ToString() + " $ ";
            lblTotal.Text = "TOTAL: " + prixFinale.ToString() + " $ ";


        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void listBoxClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            String clientSelcted = listBoxClient.SelectedItem.ToString();
            lblFactureC.Text = "Facture Pour " + clientSelcted;
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
    
}
