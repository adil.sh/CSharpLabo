﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Meteo
{
    public partial class Form1 : Form {

        // array for monthly data
        // array for cars
        private Meteo[] data;

        public struct Meteo
    {
        private int month;
        private double totalMonthlyRain;
        private double tempMaxM;
        private double tempMinM;
        private double tempAvgM;
            public static int nbVoiture;
         

            // constructor

            public Meteo (int month, double totalMonthlyRain, double tempMaxM, double tempMinM, double tempAvgM)
            {
                this.month = month;
                this.totalMonthlyRain = totalMonthlyRain;
                this.tempMaxM = tempMaxM;
                this.tempMinM = tempMinM;
                this.tempAvgM = tempAvgM;
                nbVoiture += 1;
              
            }

            // getters and setters

            public int Month
            {
                get { return this.month; }
                set
                {
                    if (value < 1 || value > 12)
                    {
                        MessageBox.Show("Please enter a value between 1 and 12");
                    }
                    else
                    {
                        this.month = value;
                    }
                }

            }

            public double TotalMonthlyRain
            {
                get { return this.totalMonthlyRain; }
                set
                {
                    if (value < 0)
                    {
                        MessageBox.Show("Please enter a valid value");
                    }
                    else
                    {
                        this.totalMonthlyRain = value;
                    }
                }
            }

            public double TempMax
            {
                get { return this.tempMaxM; }
                set
                {
                    if (value > 35)
                    {
                        MessageBox.Show("Temp cannot exceed 35 degrees");
                    }
                    else
                    {
                        this.tempMaxM = value;
                    }
                }
            }
            public double TempMin
            {
                get { return this.tempMinM; }
                set
                {
                    if (value < -35)
                    {
                        MessageBox.Show("Temp cannot be lower than -35 degrees");
                    }
                    else
                    {
                        this.tempMinM = value;
                    }
                }
            }
            public double TempAvg
            {
                get { return this.tempAvgM; }
                set
                {
                    double tempA = (tempMaxM + tempMinM) / 2;

                    this.tempAvgM = tempA;
                }
            }

        }
    
        public Form1()
        {
            InitializeComponent();
            data = new Meteo[12];
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public double rain;
        public double avgRain;

        private void butSave_Click(object sender, EventArgs e)
        {
            Meteo m1 = new Meteo(Convert.ToInt16(txtMonth.Text), Convert.ToInt16(txtRain.Text), Convert.ToDouble(txtTempH.Text), Convert.ToDouble(txtTempL.Text), ((Convert.ToDouble(txtTempH.Text) - Convert.ToDouble(txtTempL.Text)) / 2));

            data[Meteo.nbVoiture - 1] = m1;

            foreach(Meteo element in data)
            {
                listBox1.Items.Add(" Month" + element.Month + "Total Rain " + element.TotalMonthlyRain + " Highest Temp: " + element.TempMax + " Lowest Temp: " + element.TempMin + " Avg: " + element.TempAvg );
              
            }



            rain += m1.TotalMonthlyRain;
            avgRain = rain / Meteo.nbVoiture;
            avgRain = Math.Round(avgRain, 2);
            lblTotalRain.Text = "Total Yearly Rain : " + rain;

            lblYearlyRainAvg.Text = "Average Yearly Rain : " + avgRain;


         

        }
    }

}
