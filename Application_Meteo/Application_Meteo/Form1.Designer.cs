﻿namespace Application_Meteo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butSave = new System.Windows.Forms.Button();
            this.txtTempA = new System.Windows.Forms.TextBox();
            this.txtTempL = new System.Windows.Forms.TextBox();
            this.txtTempH = new System.Windows.Forms.TextBox();
            this.txtRain = new System.Windows.Forms.TextBox();
            this.txtMonth = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lblTotalRain = new System.Windows.Forms.Label();
            this.lblYearlyRainAvg = new System.Windows.Forms.Label();
            this.lblYearlyTempL = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(352, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butSave);
            this.panel1.Controls.Add(this.txtTempA);
            this.panel1.Controls.Add(this.txtTempL);
            this.panel1.Controls.Add(this.txtTempH);
            this.panel1.Controls.Add(this.txtRain);
            this.panel1.Controls.Add(this.txtMonth);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(57, 87);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(311, 265);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // butSave
            // 
            this.butSave.Location = new System.Drawing.Point(90, 223);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(139, 39);
            this.butSave.TabIndex = 11;
            this.butSave.Text = "Save";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // txtTempA
            // 
            this.txtTempA.Location = new System.Drawing.Point(178, 179);
            this.txtTempA.Name = "txtTempA";
            this.txtTempA.Size = new System.Drawing.Size(116, 20);
            this.txtTempA.TabIndex = 10;
            // 
            // txtTempL
            // 
            this.txtTempL.Location = new System.Drawing.Point(178, 140);
            this.txtTempL.Name = "txtTempL";
            this.txtTempL.Size = new System.Drawing.Size(116, 20);
            this.txtTempL.TabIndex = 9;
            // 
            // txtTempH
            // 
            this.txtTempH.Location = new System.Drawing.Point(178, 106);
            this.txtTempH.Name = "txtTempH";
            this.txtTempH.Size = new System.Drawing.Size(116, 20);
            this.txtTempH.TabIndex = 8;
            // 
            // txtRain
            // 
            this.txtRain.Location = new System.Drawing.Point(178, 70);
            this.txtRain.Name = "txtRain";
            this.txtRain.Size = new System.Drawing.Size(116, 20);
            this.txtRain.TabIndex = 7;
            // 
            // txtMonth
            // 
            this.txtMonth.Location = new System.Drawing.Point(178, 37);
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(116, 20);
            this.txtMonth.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Monthly Average";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Lowest Monthly Temp\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Highest Monthly Temp";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total Montly Rain";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Month";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblYearlyTempL);
            this.panel2.Controls.Add(this.lblYearlyRainAvg);
            this.panel2.Controls.Add(this.lblTotalRain);
            this.panel2.Controls.Add(this.listBox1);
            this.panel2.Location = new System.Drawing.Point(446, 87);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(287, 262);
            this.panel2.TabIndex = 2;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(3, 3);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(284, 121);
            this.listBox1.TabIndex = 0;
            // 
            // lblTotalRain
            // 
            this.lblTotalRain.AutoSize = true;
            this.lblTotalRain.Location = new System.Drawing.Point(16, 140);
            this.lblTotalRain.Name = "lblTotalRain";
            this.lblTotalRain.Size = new System.Drawing.Size(97, 13);
            this.lblTotalRain.TabIndex = 3;
            this.lblTotalRain.Text = "Total Yearly Rain : ";
            // 
            // lblYearlyRainAvg
            // 
            this.lblYearlyRainAvg.AutoSize = true;
            this.lblYearlyRainAvg.Location = new System.Drawing.Point(16, 166);
            this.lblYearlyRainAvg.Name = "lblYearlyRainAvg";
            this.lblYearlyRainAvg.Size = new System.Drawing.Size(113, 13);
            this.lblYearlyRainAvg.TabIndex = 4;
            this.lblYearlyRainAvg.Text = "Average Yearly Rain : ";
            // 
            // lblYearlyTempL
            // 
            this.lblYearlyTempL.AutoSize = true;
            this.lblYearlyTempL.Location = new System.Drawing.Point(16, 196);
            this.lblYearlyTempL.Name = "lblYearlyTempL";
            this.lblYearlyTempL.Size = new System.Drawing.Size(103, 13);
            this.lblYearlyTempL.TabIndex = 5;
            this.lblYearlyTempL.Text = "Lowest Yearly Temp";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 525);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtTempH;
        private System.Windows.Forms.TextBox txtRain;
        private System.Windows.Forms.TextBox txtMonth;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.TextBox txtTempA;
        private System.Windows.Forms.TextBox txtTempL;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label lblYearlyTempL;
        private System.Windows.Forms.Label lblYearlyRainAvg;
        private System.Windows.Forms.Label lblTotalRain;
    }
}

