﻿namespace _1.Fichiers_texte
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblnb = new System.Windows.Forms.Label();
            this.txtVitesse = new System.Windows.Forms.TextBox();
            this.txtKm = new System.Windows.Forms.TextBox();
            this.txtAnnee = new System.Windows.Forms.TextBox();
            this.txtFab = new System.Windows.Forms.TextBox();
            this.save = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.radVmax = new System.Windows.Forms.RadioButton();
            this.radKM = new System.Windows.Forms.RadioButton();
            this.radAnnee = new System.Windows.Forms.RadioButton();
            this.radFab = new System.Windows.Forms.RadioButton();
            this.listBoxInfo = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.lblnb);
            this.panel1.Controls.Add(this.txtVitesse);
            this.panel1.Controls.Add(this.txtKm);
            this.panel1.Controls.Add(this.txtAnnee);
            this.panel1.Controls.Add(this.txtFab);
            this.panel1.Controls.Add(this.save);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(48, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(301, 275);
            this.panel1.TabIndex = 0;
            // 
            // lblnb
            // 
            this.lblnb.AutoSize = true;
            this.lblnb.Location = new System.Drawing.Point(39, 171);
            this.lblnb.Name = "lblnb";
            this.lblnb.Size = new System.Drawing.Size(13, 13);
            this.lblnb.TabIndex = 9;
            this.lblnb.Text = "0";
            // 
            // txtVitesse
            // 
            this.txtVitesse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVitesse.Location = new System.Drawing.Point(145, 133);
            this.txtVitesse.Name = "txtVitesse";
            this.txtVitesse.Size = new System.Drawing.Size(120, 22);
            this.txtVitesse.TabIndex = 8;
            this.txtVitesse.TextChanged += new System.EventHandler(this.txtVitesse_TextChanged);
            // 
            // txtKm
            // 
            this.txtKm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKm.Location = new System.Drawing.Point(145, 105);
            this.txtKm.Name = "txtKm";
            this.txtKm.Size = new System.Drawing.Size(120, 22);
            this.txtKm.TabIndex = 7;
            // 
            // txtAnnee
            // 
            this.txtAnnee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnnee.Location = new System.Drawing.Point(145, 72);
            this.txtAnnee.Name = "txtAnnee";
            this.txtAnnee.Size = new System.Drawing.Size(120, 22);
            this.txtAnnee.TabIndex = 6;
            // 
            // txtFab
            // 
            this.txtFab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFab.Location = new System.Drawing.Point(145, 39);
            this.txtFab.Name = "txtFab";
            this.txtFab.Size = new System.Drawing.Size(120, 22);
            this.txtFab.TabIndex = 5;
            // 
            // save
            // 
            this.save.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.save.Location = new System.Drawing.Point(101, 200);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(112, 41);
            this.save.TabIndex = 4;
            this.save.Text = "Sauvegarder";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Vitesse Max:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "KM: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Annee:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fabriquant: ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.Controls.Add(this.txtSearch);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.radVmax);
            this.panel2.Controls.Add(this.radKM);
            this.panel2.Controls.Add(this.radAnnee);
            this.panel2.Controls.Add(this.radFab);
            this.panel2.Controls.Add(this.listBoxInfo);
            this.panel2.Location = new System.Drawing.Point(436, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(296, 275);
            this.panel2.TabIndex = 1;
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(132, 236);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(116, 22);
            this.txtSearch.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(30, 236);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 27);
            this.button1.TabIndex = 5;
            this.button1.Text = "Recherche...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // radVmax
            // 
            this.radVmax.AutoSize = true;
            this.radVmax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radVmax.Location = new System.Drawing.Point(134, 200);
            this.radVmax.Name = "radVmax";
            this.radVmax.Size = new System.Drawing.Size(99, 20);
            this.radVmax.TabIndex = 4;
            this.radVmax.TabStop = true;
            this.radVmax.Text = "Vitesse Max";
            this.radVmax.UseVisualStyleBackColor = true;
            // 
            // radKM
            // 
            this.radKM.AutoSize = true;
            this.radKM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radKM.Location = new System.Drawing.Point(134, 165);
            this.radKM.Name = "radKM";
            this.radKM.Size = new System.Drawing.Size(45, 20);
            this.radKM.TabIndex = 3;
            this.radKM.TabStop = true;
            this.radKM.Text = "KM";
            this.radKM.UseVisualStyleBackColor = true;
            // 
            // radAnnee
            // 
            this.radAnnee.AutoSize = true;
            this.radAnnee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radAnnee.Location = new System.Drawing.Point(30, 200);
            this.radAnnee.Name = "radAnnee";
            this.radAnnee.Size = new System.Drawing.Size(65, 20);
            this.radAnnee.TabIndex = 2;
            this.radAnnee.TabStop = true;
            this.radAnnee.Text = "Annee";
            this.radAnnee.UseVisualStyleBackColor = true;
            // 
            // radFab
            // 
            this.radFab.AutoSize = true;
            this.radFab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radFab.Location = new System.Drawing.Point(30, 165);
            this.radFab.Name = "radFab";
            this.radFab.Size = new System.Drawing.Size(90, 20);
            this.radFab.TabIndex = 1;
            this.radFab.TabStop = true;
            this.radFab.Text = "Fabriquant";
            this.radFab.UseVisualStyleBackColor = true;
            // 
            // listBoxInfo
            // 
            this.listBoxInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxInfo.FormattingEnabled = true;
            this.listBoxInfo.ItemHeight = 16;
            this.listBoxInfo.Location = new System.Drawing.Point(3, 0);
            this.listBoxInfo.Name = "listBoxInfo";
            this.listBoxInfo.Size = new System.Drawing.Size(290, 148);
            this.listBoxInfo.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(754, 384);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtVitesse;
        private System.Windows.Forms.TextBox txtKm;
        private System.Windows.Forms.TextBox txtAnnee;
        private System.Windows.Forms.TextBox txtFab;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radVmax;
        private System.Windows.Forms.RadioButton radKM;
        private System.Windows.Forms.RadioButton radAnnee;
        private System.Windows.Forms.RadioButton radFab;
        private System.Windows.Forms.ListBox listBoxInfo;
        private System.Windows.Forms.Label lblnb;
    }
}

