﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1.Fichiers_texte
{
    public partial class Form1 : Form
    {
        // array for cars
        private voiture[] inventory;

        //create a structure
        private struct voiture
        {
            private string fab;
            private int annee, km, vMax;
            public static int nbVoiture;

            // constructor 
            public voiture(string fab, int annee, int km, int vMax)
            {
                this.fab = fab;
                this.annee = annee;
                this.km = km;
                this.vMax = vMax;
                nbVoiture+= 1;
            }

            // create a property for getter and setter
            public string FAB
            {
                get { return this.fab; }

                set
                {
                    if (value == "Toyota")
                    {
                        MessageBox.Show("On ne achete pas des Toyota");
                    }
                    else
                    {
                        this.fab = value;
                    }
                }
            }

            public int ANNEE
            {
                get { return this.annee; }
                set
                {
                    if (value < 2005)
                    {
                        MessageBox.Show("La voiture doit etre 2005 ou plus");

                    }
                    else if (value > 2018)
                    {
                        this.annee = 2018;
                    }

                    else
                    {
                        this.annee = value;
                    }
                }
            }

            public int KM
            {
                get { return this.km; }

                set
                {
                    if (value > 250)
                    {
                        MessageBox.Show("La voiture a trop des km (max 250km)");
                    }
                    else
                    {
                        this.km = value;
                    }
                }
            }

            public int VMAX
            {
                get { return this.vMax; }

                set
                {
                    if(value < 120)
                    {
                        MessageBox.Show("La vitesse Maximum doit etre plus que 120");
                    }
                    else
                    {
                        this.vMax = value;
                    }
                }
            }
        }
            public Form1()
            {
                InitializeComponent();
                inventory = new voiture[10];
            }

            private void Form1_Load(object sender, EventArgs e)
            {

            }

            private void txtVitesse_TextChanged(object sender, EventArgs e)
            {

            }

            private void save_Click(object sender, EventArgs e)
            {
                voiture v1 = new voiture(txtFab.Text, Convert.ToInt16(txtAnnee.Text), Convert.ToInt16(txtKm.Text),
                Convert.ToInt16(txtVitesse.Text));


                inventory[voiture.nbVoiture - 1] = v1;
                lblnb.Text = voiture.nbVoiture.ToString();
             
            }

        private void button1_Click(object sender, EventArgs e)
        {
            if(radFab.Checked)
            {
               foreach (voiture element in inventory)
                {
                    if (element.FAB == txtSearch.Text)
                    {
                        listBoxInfo.Items.Add(element.FAB);
                        listBoxInfo.Items.Add(element.ANNEE);
                        listBoxInfo.Items.Add(element.KM);
                        listBoxInfo.Items.Add(element.VMAX);
                    }

                  
                }
            }
            else if (radAnnee.Checked)
            {
                foreach (voiture element in inventory)
                {
                    if (element.ANNEE == Convert.ToInt16(txtSearch.Text))
                    {

                        listBoxInfo.Items.Add(element.FAB);
                        listBoxInfo.Items.Add(element.ANNEE);
                        listBoxInfo.Items.Add(element.KM);
                        listBoxInfo.Items.Add(element.VMAX);
                    }
                  
                }
            }

            else if (radKM.Checked)
            {
                foreach (voiture element in inventory)
                {
                    if (element.KM == Convert.ToInt16(txtSearch.Text))
                    {

                        listBoxInfo.Items.Add(element.FAB);
                        listBoxInfo.Items.Add(element.ANNEE);
                        listBoxInfo.Items.Add(element.KM);
                        listBoxInfo.Items.Add(element.VMAX);
                    }
                   
                }
            }

            else if (radVmax.Checked)
            {
                foreach (voiture element in inventory)
                {
                    if (element.VMAX == Convert.ToInt16(txtSearch.Text))
                    {

                        listBoxInfo.Items.Add(element.FAB);
                        listBoxInfo.Items.Add(element.ANNEE);
                        listBoxInfo.Items.Add(element.KM);
                        listBoxInfo.Items.Add(element.VMAX);
                    }
                  
                }
            }
        }
    }
}

